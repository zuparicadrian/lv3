﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Generator
    {
        private static Generator instance;
        private Random generator = new Random();
        public double[,] matrix;
        public static Generator GetInstance()
        {
            if (instance == null)
            {
                instance = new Generator();
            }
            return instance;
        }
        private Generator()
        {
            matrix = new double[0, 0];
        }

        public void Print()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i,j]);
                }
                Console.WriteLine();
            }
        }
        public void Fill()
        {
            int i, j;
            for (i = 0; i < matrix.GetLength(0); i++)
            {
                for (j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = generator.NextDouble();
                }
            }
        }
        public void a(int redovi, int stupci)
        {
            matrix = new double[redovi, stupci];
        }
    }
}
