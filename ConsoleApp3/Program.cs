﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            Dataset data1 = new Dataset("text.csv");
            Dataset data2 = (Dataset)data1.Clone();
            data1.Print();
            data2.Print();

            Generator generator = Generator.GetInstance();
            generator.a(5, 5);
            generator.Fill();
            generator.Print();

            ConsoleNotification Firsttext = new ConsoleNotification("Adrian Župarić", "Naslov", "Tekst", DateTime.Now, Category.INFO, ConsoleColor.Magenta);
            NotificationManager Text = new NotificationManager();

            Text.Display(Firsttext);

        }
    }
}
